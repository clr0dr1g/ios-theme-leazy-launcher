
var obj = {};

$(function(){

	Leazy.onBackPress(function(){
		Leazy.restart();
	});

	var b = $('.bottom-bar');
	var c = $('.swiper-container');

	var bH = b.height();
	var cH = c.height();

	var nH = Leazy.getHeightNavBar();

	b.height(bH + nH);
	c.height(cH - nH);

	var slideHeight = $('.swiper-slide').height();
	var spaceToStatusBar = Leazy.getHeightStatusBar() + 20;
	$('.swiper-slide').css({
		'padding-top': spaceToStatusBar,
		'height': slideHeight - spaceToStatusBar
	});

	$('.swiper-slide').eq(0).append('<div class="app"><div class="img"></div><span>Aplicativo</span></div>');
	var appwidth = $('.swiper-slide .app').width();
	var appHeight = appwidth;
	$('.swiper-slide .app').css({
		"width": appwidth,
		"height": appHeight
	});

	var swiperHeight = $('.swiper-slide').height();
	var appsPerPage = Math.floor(swiperHeight / appwidth);
	appsPerPage = Math.floor(appsPerPage * 4);

	Leazy.log(appsPerPage);

	var allApps = Leazy.getAppList();
	var contInstalledApps = allApps.length;

	var loops = Math.ceil(contInstalledApps / appsPerPage);

	$('.swiper-slide').remove();

	for(var i = 0; i < loops; i++){
		$('.swiper-wrapper').append('<div class="swiper-slide full"></div>');
		for(var x = 0; x < appsPerPage; x++){
			var currentApp = (i * appsPerPage) + x;
			var app = allApps[currentApp];
			if(app == null){
				x = appsPerPage;
				i = loops;
			}else{
				$('.swiper-slide').eq(i).append('<div class="app" data-package="' + app.packageName + '"><img class="img" src="' + app.icon + '" /><span>' + app.name + '</span></div>');
			}
		}
	}

	$('.swiper-slide').css({
		'padding-top': spaceToStatusBar,
		'height': slideHeight - spaceToStatusBar
	});

	$('.swiper-slide .app').css({
		"width": appwidth,
		"height": appHeight
	});

	obj.swiper = new Swiper('.swiper1', {
		pagination: '.swiper-pagination',
		paginationClickable: true
	});

	Leazy.onHomePress(function(){
		obj.swiper.slideTo(0);
	});

	$('.app').click(function() {
		var pack = $(this).attr("data-package");
		Leazy.runApp(pack);
	});

});